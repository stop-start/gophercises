/*
Exercise 1 - Quiz:
https://gophercises.com/exercises/quiz
or
https://github.com/gophercises/quiz
*/
package main

import (
	"flag"

	"os"

	"time"

	"bitbucket.org/stop-start/gophercises/Exercise1_Quiz/quiz"
)

//------------------------------------------------------------------------------
func main() {
	var (
		quizFileName = flag.String("csv", "problems.csv", "--csv <file>")
		timeLimit    = flag.Duration("time-limit", time.Second*30, "--time-limit <seconds>")
	)
	flag.Parse()

	var (
		q        *quiz.Quiz
		quizFile *os.File
		err      error
	)
	if quizFile, err = os.Open(*quizFileName); err != nil {
		panic(err.Error())
	}

	if q, err = quiz.NewQuiz(quizFile, *timeLimit); err != nil {
		panic("error while initializing the quiz: " + err.Error())
	}

	q.Start()
}
