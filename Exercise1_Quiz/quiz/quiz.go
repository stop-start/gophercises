/*
Exercise 1 - Quiz:
https://gophercises.com/exercises/quiz
or
https://github.com/gophercises/quiz
*/
package quiz

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"
)

//------------------------------------------------------------------------------
//Quiz holds the question/answer pairs, time limit and results of the quiz
type Quiz struct {
	qaPairs        []*questionAnswerPair
	correctAnswers int
	timeLimit      time.Duration
}

type questionAnswerPair struct {
	question string
	answer   string
}

type userAnswer struct {
	answerChan  chan string
	timesUPChan chan struct{}
}

//------------------------------------------------------------------------------
var ErrEmptyQuiz = errors.New("empty quiz")
var ErrCorruptedQuiz = errors.New("corrupted quiz")

//------------------------------------------------------------------------------
//NewQuiz returns a new Quiz struct based on the reader and time limit passed
func NewQuiz(quizFile io.Reader, timeLimit time.Duration) (*Quiz, error) {
	// ---- Because "You can assume that quizzes will be relatively short (< 100 questions)"
	// ---- All Q&A will be read into a struct before starting the quiz
	qas, err := readCSV(quizFile)
	if err != nil {
		return nil, err
	}

	return &Quiz{
		qaPairs:        qas,
		correctAnswers: 0,
		timeLimit:      timeLimit,
	}, nil
}

//------------------------------------------------------------------------------
func readCSV(f io.Reader) ([]*questionAnswerPair, error) {
	r := csv.NewReader(f)
	qaList, err := r.ReadAll()
	if err != nil {
		return nil, err
	}
	if len(qaList) == 0 {
		return nil, ErrEmptyQuiz
	}

	qas := make([]*questionAnswerPair, len(qaList))
	for i, pair := range qaList {
		if len(pair) < 2 {
			return nil, ErrCorruptedQuiz
		}
		qas[i] = &questionAnswerPair{question: pair[0], answer: pair[1]}

	}
	return qas, nil
}

//------------------------------------------------------------------------------
func doQuestion(reader *bufio.Reader, question string, answer *userAnswer) {
	fmt.Println(question)
	select {
	case <-answer.timesUPChan:
		return
	default:
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Fprintln(os.Stderr, "error while reading from stdin:", err)
			answer.answerChan <- ""
		}
		answer.answerChan <- strings.TrimSpace(input)
	}

}

//------------------------------------------------------------------------------
func (q *Quiz) Start() {
	ans := &userAnswer{
		answerChan:  make(chan string),
		timesUPChan: make(chan struct{}),
	}
	reader := bufio.NewReader(os.Stdin)
	timesUP := time.After(q.timeLimit)

QUIZ:
	for _, qa := range q.qaPairs {
		go doQuestion(reader, qa.question, ans)
		select {
		case <-timesUP:
			fmt.Println("Time's up!")
			close(ans.timesUPChan)
			break QUIZ
		case answer := <-ans.answerChan:
			if answer == qa.answer {
				q.correctAnswers++
			}
		}
	}

	fmt.Printf("Results: %d/%d\n", q.correctAnswers, len(q.qaPairs))
}
