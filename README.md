# README #

This is a repo with my solutions to Jon Calhoun's exercises from [the gophercises website](https://gophercises.com).  
Each exercise is in its own folder. A link to the original exercise can be found 
in the comments at the top of each 'go' file.
